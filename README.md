# CS5024 Blog

## Writing Assigment 10: Preserving Social Norms in VR

Before discussing social norms in the context of VR, it is important to understand the current state of VR. As it stands, VR is semi-immersive at best with most users experiencing VR through a headset and set of remotes. VR for these users is primarily used for entertainment in the form of games and augmented social interactions. A smaller subset of users have access to more sophisticated, but still only semi-immersive, simulators for things like training (think flight simulators). Because VR in its current form is still somewhat rudimentary, and its role is primarily entertainment, It is not crucial to strictly enforce cultural norms. We already adopt this policy with other forms of entertainment like video games, movies, and music that has sensitive content. Individuals are free to not engage with the offending media.

As advancements are made in human-computer interfaces, VR has the potential to become fully immersive. Moreover, there is also the possibility of VR becoming more integrated in our lives (e.g. using VR commonly to fulfill work duties). As VR proliferates along these two dimensions, preserving social norms within VR becomes a much more pertinent question. At this point, it is not clear if our current laws and policies are suitable for direct adaption to immersive VR environments. Many immoral actions mentioned in lecture have a clear victim in reality. When some of these actions lose their victim, it is unclear if they should be punished and looked down upon in the same way as their real counterparts. This is a complicated issue and I imagine that many cases will work their way up the judicial system.


## Writing Assignment 9: Frankenstein syndrome

I am less worried about the threat of artificial general intelligence (AGI) and more concerned about the little ways artificial intelligence impacts our current lives. We clearly do not have to contend with AGI today and, while AI is clearly improving, we likely require one or more major breakthroughs to achieve a creation that could pose an existential threat. Moreover, i think it is difficult to estimate the threat of AGI and we consistently ask the wrong people to estimate that threat (tech CEOS).

What is more concerning is the huge amount of data and influence that large tech companies wield today to manipulate consumers in subtle and often innocuous ways. Artificial intelligence in some form is quietly influencing our employment (Amazon hiring AI), education (GRE essay grading assignment), financial services (AI in insurance pricing), etc. Before worrying about applying our social norms to AGI we should worry about reigning in today’s AI.


## Writing Assignment 8: Social Media and Cyberbullying

All issues of moderation are tricky on social media platforms. Controllers of said platforms must strike a balance between allowing potentially damaging rhetoric and engaging in censorship that could be equally damaging by nature of its biased application. Just like other forms of sensitive content, most people can agree that social media would be improved by the elimination of cyberbullying and online harassment. Unfortunately, agreements fall apart when it comes to classifying and measuring the magnitude of particular instances of cyberbullying. Social media platforms have the opportunity to tackle cyberbullying from two angles: centralized policies implemented by platforms themselves or tools given to users to allow for self-regulation.

Centralized policies include rules against cyberbullying that are enforced by the social media platform. Enforcement of such policies places a burden on the social media platform. The platform becomes responsible for maintaining the technologies and personnel required for surveillance and identification and subsequent removal of offending material. An example of a more decentralized policy is giving users finer grained control over the content that gets displayed to them. For example, giving users the ability to block content from other individuals could be a small tool in mitigating cyberbullying. I believe a comprehensive combination of centralized and decentralized policies are required to make an effective attempt at reducing cyberbullying.


## Writing Assignment 7: Stopping Discriminatory Algorithms

I largely agree with the perspective that algorithms are fair and data, as a reflection of our biased reality, is the true culprit in many “discriminatory algorithm” cases. This is especially true when considering that most systems accused of programmatic discrimination incorporate some sort of machine learning method. These machine learning methods are trained on biased input data and learn to reflect and in some cases reproduce or uphold the discriminatory trends that are picked up from the data. One commonly referenced example is Amazon’s discriminatory algorithm for hiring software engineers.

Restricted access to unbiased datasets is the achilles heel of our machine learning driven society. Algorithms trained on this data have the potential to do harm in a very widespread and systematic manner. This is especially problematic because policy makers and even computer scientists have a greater tendency to hold algorithms above reproach as perfectly fair, object arbiters and decision makers. Moving forward, concerted efforts must be made to not leak biased reality into the machine learning algorithms that govern our health care, financial services, justice systems, and so much more.


## Writing Assignment 6: Service-level Agreements

Service-level Agreements (SLA) are an imperfect solution in the context of software. Compared to other domains, software is both complex and dynamic. 

There are several reasons why the complexity of a software product could be intractable to its providers and customers. First, a software provider could keep their source code proprietary and either ship binaries to its customers or host the software on cloud services for use by customers. Second, even if providers disclose their source code, many customers lack the expertise and resources to evaluate the quality of the source. Finally, the provider could easily make mistakes and fail to cover an edge case that is implied as part of an SLA.

Software products are maintained and transformed in response to the changing needs and infrastructure of its customers. Therefore, an SLA that accurately outlined a contract between providers and customers at one point in time may quickly grow outdated. Moreover, the cost of accurately maintaining an SLA might hinder the ability of providers to fluidly respond to the needs of their customers. Finally, mismatches between a software product’s features and its description in an SLA could result in miscommunication and dissatisfaction between providers and customers.

Service-level Agreements offer some protection for consumers but are often cumbersome and difficult to evaluate.


## Writing Assignment 5: Data Ownership and Business Bankruptcy

Data ownership largely depends on the circumstances of its collection. Most people can agree that expectations for privacy differ when it comes to your actions at home and your actions in public. By the same token, data can be collected in myriad ways with consequences on who owns and controls the data.

Where this philosophy falls apart is situations where combining data reveals emergent value. In other words, a user may consent to producing data for multiple counterparties with the expectation that their data would remain siloed. In actuality, this data could be combined to reveal more than the user expected or is comfortable with. A modern example is linking amazon purchase history with facebook data. Similarly, combination of data among multiple users within a single application can have a similar effect. To illustrate, using their friends' data, it is possible to discover demographic and political information for facebook users who have chosen to be protective of their information.

Using RadioShack as a case study, the State attorney generals stopping the sale of data was ethical. Despite their impending bankruptcy, RadioShack had no right to sell customer data because customers reasonably believed that the data would not be sold. The expectations surrounding the data’s collection dictate its proper use.


## Writing Assignment 4: Human Subject Research


In my junior year of my undergraduate career at Virginia Tech, I participated in a graduate students HCI thesis study as part of my HCI class. This graduate student was attempting to create an interactive display that improved in-person collaboration on data science tasks. Another student and I were asked to use a prototype of the display to complete a series of analysis tasks on a corpus of data taken from twitter. The prototype featured a projector aimed downward at a table. My partner and I were able to manipulate projected by touching and sliding them to each other. To illustrate, we were asked to do something along the lines of determine what portion of the available tweets used the #outdoors hashtag given a collection of blobs of tweets with the same hashtags that were sized according to how many tweets were in the group. We were timed in our completion of these tasks and our answers were required presumably for review after the completion of our session. In addition to our session, we filled out pre and post surveys to gauge our confidence and experience.

My interactions with VT IRB were fairly limited. As a participant in the study, I needed to fill out an informed consent form.


## Writing Assignment 3: Personal Ethical

One of the most challenging ethical dilemmas of my life was the decision to euthanize my cat. I had a sphynx cat growing up that I was very fond of. She had a great personality and felt like a very important member of my nuclear family. I had plans to take the cat with me wherever I ended up after finishing my post-secondary education. Unfortunately, those plans were cut short by the onset of a condition in my cat’s heart. We came to understand that the condition, hypertrophic cardiomyopathy, had increased prevalence in some breeds of cats including sphynx cats.

For a little over a month, we attempted to care for the cat as best as possible. We would administer the prescribed medication and hand feed her. Despite these measures, It was clear that her condition wasn’t improving. Towards the end, it was clear that her breathing was labored. The ethical dilemma we faced was in weighing the value of her remaining life against the suffering she would endure. This is a tough decision to make due to our own biases and the cat’s inability to articulate her feelings. I’m still not sure if we made the right decision. Despite her clear suffering, I remember her being so alert and alive on the day we euthanized her. I still think about his dilemma often and hope that If I make a similar decision in the future that I am more confident about the result.

 

## Writing Assignment 2: Ethical Theories and My Morality

Like many others, I mostly closely follow the utilitarian theory when weighing my actions as good or bad. One of the main points of conflict between those who practice utilitarianism is the assignment of utility or value to a particular action. Moreover, as imperfect actors, we all miscalculate the exact nature of an action's consequences from time to time. Furthermore, utilitarianism fails to account for the intent of actors which I believe is an important factor to consider in any ethical framework. Despite these shortcomings, I rely on my own flavor of utilitarianism to judge and plan my own actions.

From day to day, this perspective manifests in small values and habits. The first of which is my disdain for litter. The act of littering is a perfect example of Egoism in the sense that a single individual sacrifices some small portion of the environment’s beauty and health in favor of their own convenience. Another small action governed by this line of utilitarian thinking is the importance I put on returning the grocery cart to its proper place after finishing shopping (the sum of the inconvenience to each individual putting away their cart is less than the inconvenience to the single person who would have to gather everyone's scattered carts). These small actions represent the value I place on maximizing the good I impart on a day-to-day basis.


## Writing Assignment 1: My Values and Principles
### Values

The following are some of my most important values.

##### Honesty

My commitment to honesty started with my upbrining and evolved due to my participation
in the graduate program and Virginia Tech. My parents encouraged honesty in both my academic
pursuits and other aspects of my life. Working under Professor Margaret Ellis and Dr. Kirk Cameron
in my undergraduate and graduate research strengthened this value. My advisors and supervisors have
instilled in me a strong appreciation and respect for intellectual honesty when it comes to conducting research.

##### Open-mindedness

My commitment to open-mindedness started during my secondary education and continued to solidify
throughout my undergraduate and graduate careers. Attending a large university with a diverse student
body like Virginia Tech exposed me to many perspectives and emboldened my stance that it is important
to consider the thoughts of others. Reviewing papers as part of undergraduate and graduate research has also demonstrated the importance of considering and learning from the perspectives of others.


### Principles

The following are some of my most important principles that fall out of my values.

##### Remain transparent in my communications

My principle of being transparent in my communications comes directly from my commitment to honesty. Transparent communications
facilitate the knowledge sharing that is required to do effective research in a group. It is equally important to communicate good
and bad outcomes so that innovation can continue unhindered. This principle has served me well in my work thus far.


##### Seek to understand others' points of view

My principle of attempting to understand other points of view is also a boon to my ability to do research. Making a concerted effort
to understand other perspectives enables me to collaborate more effectively and more easily digest the literature in my area of focus.